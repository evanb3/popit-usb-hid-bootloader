/**************************************************************************************************
* File: SF_driver.c
* File Type: C - Source File
* Company: (c) mikroElektronika, 2011
* Revision History:
*       - initial release
* Description:
*     This module contains a set of functions that are used for communication with
*     Serial Flash.
* Test configuration:
     MCU:             LM3S9b95
                      http://www.ti.com/lit/ds/symlink/lm3s9b95.pdf
     dev.board:       EasyMx v7 for STELLARIS(R) ARM(R)
                      http://www.mikroe.com/eng/products/view/792/easymx-pro-v7-for-stellaris-arm/
                      

                   eri
     Oscillator:      HS-PLL, 80.000 MHz Crystal
     SW:              mikroC PRO for ARM
                      http://www.mikroe.com/eng/products/view/752/mikroc-pro-for-arm/s
* NOTES:
*     Serial Flash use SPI bus to communicate with MCU.
      Turn on SW11 and SW12 TFT control switches
*     Turn on SW13.1, SW13.2, sw13.3 and SW13.6
**************************************************************************************************/
#include <built_in.h>
#include "SF_driver.h"
#include <Config.h>

unsigned long flash_id;
unsigned char protection_reg[18];
/**************************************************************************************************
* Serial Flash Chip Select connection
**************************************************************************************************/
//sbit CS_Serial_Flash_bit           at GPIO_PORTC_DATA.B11;
//sbit CS_Serial_Flash_Direction_bit at GPIO_PORTC_DIR.B11;
/*************************************************************************************************/


void SerialFlash_Reset()
{
  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_RESET_EN);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  Delay_us(1);
  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_RESET);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  Delay_us(1000);
}


void SerialFlash_GlobalUnlock()
{
  SerialFlash_WriteEnable();
  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_GLOBAL_UNLOCK);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
}
/**************************************************************************************************
* Function SerialFlash_init()
* -------------------------------------------------------------------------------------------------
* Overview: Function that initializes SerialFlash by setting Chip select
* Input: none
* Output: none
**************************************************************************************************/
void SerialFlash_init(){
  GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_11);
  //GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_13);
  //GPIO_Digital_Output(&GPIOB_BASE, _GPIO_PINMASK_15);
  //GPIO_Digital_Input(&GPIOB_BASE, _GPIO_PINMASK_14 );
  CS_Serial_Flash_bit = 1;
  SerialFlash_Reset();
  flash_id = SerialFlash_ReadID();
  if(flash_id == SST26VF064B_ID)
    SerialFlash_GlobalUnlock();
  //CS_Serial_Flash_Direction_bit = 1;
}

/**************************************************************************************************
* Function SerialFlash_WriteEnable()
* -------------------------------------------------------------------------------------------------
* Overview: Function that sends write enable command to the chip
* Input: none
* Output: none
**************************************************************************************************/
void SerialFlash_WriteEnable(){
  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_WREN);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);
}


unsigned char SerialFlash_ReadSR()
{
  unsigned char temp;

  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_RDSR);
  //Delay_us(10);
  temp = SPI_Rd_Ptr(0);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);

  return (temp);
}

/**************************************************************************************************
* Function SerialFlash_IsWriteBusy()
* -------------------------------------------------------------------------------------------------
* Overview: Function that checks whether chip finished write operation
* Input: none
* Output: 1 - if still busy, 0 - if write completed
**************************************************************************************************/
unsigned char SerialFlash_IsWriteBusy(){
  unsigned char temp;

  temp = SerialFlash_ReadSR();

  return (temp&0x01);
}

unsigned char SerialFlash_ReadConfigReg(void)
{
  unsigned char temp;

  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_READ_CR);
  //Delay_us(10);
  temp = SPI_Rd_Ptr(0);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);

  return (temp);
}

/**************************************************************************************************
* Function SerialFlash_WriteByte()
* -------------------------------------------------------------------------------------------------
* Overview: Function that writes a single byte
* Input: Data to be written and the address to which to store the data
* Output: none
**************************************************************************************************/
void SerialFlash_WriteByte(unsigned char _data, unsigned long address){
    SerialFlash_WriteEnable();
    
    // When you do SPI2_Init() then SPI_Rd_Ptr and SPI_Wr_Ptr initialize and correspond to SPI2_Read and SPI2_Write.
    CS_Serial_Flash_bit = 0;
    //Delay_us(10);
    SPI_Wr_Ptr(_SERIAL_FLASH_CMD_WRITE);
    //Delay_us(10);
    SPI_Wr_Ptr(Higher(address));
    //Delay_us(10);
    SPI_Wr_Ptr(Hi(address));
    //Delay_us(10);
    SPI_Wr_Ptr(Lo(address));
    //Delay_us(10);
    SPI_Wr_Ptr(_data);
    //Delay_us(10);
    CS_Serial_Flash_bit = 1;
    //Delay_us(10);

    // Wait for write end
    while(SerialFlash_isWriteBusy());
}

void SerialFlash_WriteData(unsigned long address, unsigned char *pData, unsigned long len, unsigned char fill)
{
    unsigned long i, write_size = len;

    // check for page boundary crossing and adjust first write accordingly
    if((len > EXTERN_SPI_FLASH_PAGE_SIZE) || (address % EXTERN_SPI_FLASH_PAGE_SIZE + len > EXTERN_SPI_FLASH_PAGE_SIZE))
        write_size = EXTERN_SPI_FLASH_PAGE_SIZE - address % EXTERN_SPI_FLASH_PAGE_SIZE;
    while(len)
    {
        SerialFlash_WriteEnable();

        CS_Serial_Flash_bit = 0;
        // copy the page to buffer 1
        SPI_Wr_Ptr(_SERIAL_FLASH_CMD_WRITE);
        SPI_Wr_Ptr(Higher(address));
        SPI_Wr_Ptr(Hi(address));
        SPI_Wr_Ptr(Lo(address));
        for(i=0; i < write_size; i++)
        {
         if(pData != 0)
            SPI_Wr_Ptr(*pData++);
         else
             SPI_Wr_Ptr(fill);
        }
        CS_Serial_Flash_bit = 1;
        // wait for write complete
        while(SerialFlash_isWriteBusy());

        address += write_size;
        len -= write_size;
        // calc next write size
        write_size = (len > EXTERN_SPI_FLASH_PAGE_SIZE) ? EXTERN_SPI_FLASH_PAGE_SIZE : len;
     }

     //return true;
}
    
/**************************************************************************************************
* Function SerialFlash_WriteWord()
* -------------------------------------------------------------------------------------------------
* Overview: Function that writes 2 succesive bytes of a word variable
* Input: Word data to be written and the address to which to store the data
* Output: none
**************************************************************************************************/
void SerialFlash_WriteWord(unsigned int _data, unsigned long address){
  SerialFlash_WriteByte(Hi(_data),address);
  SerialFlash_WriteByte(Lo(_data),address+1);
}

/**************************************************************************************************
* Function SerialFlash_ReadID()
* -------------------------------------------------------------------------------------------------
* Overview: Function that reads the CHIP ID
* Input: none
* Output: ID byte value
**************************************************************************************************/
unsigned long SerialFlash_ReadID(void){
  unsigned long temp;

  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_RDID);
  //Delay_us(10);
  Higher(temp) = SPI_Rd_Ptr(0);
  Hi(temp) = SPI_Rd_Ptr(0);
  Lo(temp) = SPI_Rd_Ptr(0);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);
  
  return temp;
}

/**************************************************************************************************
* Function SerialFlash_ReadByte()
* -------------------------------------------------------------------------------------------------
* Overview: Function that reads the byte from the address
* Input: address to be read
* Output: byte data from the address
**************************************************************************************************/
unsigned char SerialFlash_ReadByte(unsigned long address){
  unsigned char temp;

  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_READ);
  //Delay_us(10);
  SPI_Wr_Ptr(Higher(address));
  //Delay_us(10);
  SPI_Wr_Ptr(Hi(address));
  //Delay_us(10);
  SPI_Wr_Ptr(Lo(address));
  //Delay_us(10);
  temp = SPI_Rd_Ptr(0);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);
  return temp;
}

/**************************************************************************************************
* Function SerialFlash_ReadWord()
* -------------------------------------------------------------------------------------------------
* Overview: Function that reads the word from the address
* Input: address to be read
* Output: word data stored in two successive addresses
**************************************************************************************************/
unsigned int SerialFlash_ReadWord(unsigned long address){
  unsigned int temp;

  Hi(temp) = SerialFlash_ReadByte(address);
  Lo(temp) = SerialFlash_ReadByte(address+1);

  return temp;
}

/**************************************************************************************************
* Function SerialFlash_WriteArray()
* -------------------------------------------------------------------------------------------------
* Overview: Function that writes data to successive addresses
* Input: address of the begining, pointer to buffer containing data,
         number of bytes to be written
* Output: 1 - if write succeeded, 0 - if write failed
**************************************************************************************************/
unsigned char SerialFlash_WriteArray(unsigned long address, unsigned char* pData, unsigned int nCount){
  unsigned long addr;
  unsigned char* pD;
  unsigned int counter;

  addr = address;
  pD   = pData;

  // WRITE

  for(counter = 0; counter < nCount; counter++){
      SerialFlash_WriteByte(*pD++, addr++);
  }


  // VERIFY

  for (counter=0; counter < nCount; counter++){
    if (*pData != SerialFlash_ReadByte(address))
        return 0;
    pData++;
    address++;
  }

  return 1;
}

/**************************************************************************************************
* Function SerialFlash_ReadArray()
* -------------------------------------------------------------------------------------------------
* Overview: Function that reads data from successive addresses
* Input: address of the begining, pointer to buffer where to store read data,
         number of bytes to be read
* Output: none
**************************************************************************************************/
void SerialFlash_ReadArray(unsigned long address, unsigned char* pData, unsigned int nCount){
  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_READ);
  //Delay_us(10);
  SPI_Wr_Ptr(Higher(address));
  //Delay_us(10);
  SPI_Wr_Ptr(Hi(address));
  //Delay_us(10);
  SPI_Wr_Ptr(Lo(address));
  //Delay_us(10);
  while(nCount--){
    *pData++ = SPI_Rd_Ptr(0);
    //Delay_us(10);
  }
  CS_Serial_Flash_bit = 1;
}

void SerialFlash_ReadBlockProt()
{
  char i;
  
  CS_Serial_Flash_bit = 0;
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_READ_BPR);
  for(i=0; i <18; i++)
  {
    protection_reg[i] = SPI_Rd_Ptr(0);
  }
  CS_Serial_Flash_bit = 1;
}

/**************************************************************************************************
* Function SerialFlash_ChipErase()
* -------------------------------------------------------------------------------------------------
* Overview: Function that sends Chip Erase command
* Input: none
* Output: none
**************************************************************************************************/
void SerialFlash_ChipErase(void){

  SerialFlash_WriteEnable();

  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_ERASE);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);
  // Wait for write end
  while(SerialFlash_IsWriteBusy());
}

/**************************************************************************************************
* Function SerialFlash_ResetWriteProtection()
* -------------------------------------------------------------------------------------------------
* Overview: Function that sends Reset Write Protection command
* Input: none
* Output: none
**************************************************************************************************/
void SerialFlash_ResetWriteProtection(){

  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_EWSR);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);
  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_EWSR);
  //Delay_us(10);
  SPI_Wr_Ptr(0);
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);
}

/**************************************************************************************************
* Function SerialFlash_SectorErase()
* -------------------------------------------------------------------------------------------------
* Overview: Function that sends Sector Erase command
* Input: address of the sector to be erased
* Output: none
**************************************************************************************************/
void SerialFlash_SectorErase(unsigned long address){

  SerialFlash_WriteEnable();

  CS_Serial_Flash_bit = 0;
  //Delay_us(10);
  SPI_Wr_Ptr(_SERIAL_FLASH_CMD_SER);
  //Delay_us(10);
  SPI_Wr_Ptr(Higher(address));
  //Delay_us(10);
  SPI_Wr_Ptr(Hi(address));
  //Delay_us(10);
  SPI_Wr_Ptr(Lo(address));
  //Delay_us(10);
  CS_Serial_Flash_bit = 1;
  //Delay_us(10);
  // Wait for write end
  while(SerialFlash_IsWriteBusy());
}

/**************************************************************************************************
* End of File
**************************************************************************************************/