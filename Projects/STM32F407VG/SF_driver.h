/**************************************************************************************************
* File: SF_driver.h
* File Type: C - Header File
* Company: (c) mikroElektronika, 2011
* Revision History:
*     20111115 (DA):
*       - initial release;
*     20111117 (JK);
*       - revision;
* Description:
*     This module contains a set of functions that are used for communication with
*     Serial Flash.
* Test configuration:
     MCU:             LM3S9b95
                      http://www.ti.com/lit/ds/symlink/lm3s9b95.pdf
     dev.board:       EasyMx v7 for STELLARIS(R) ARM(R)
                      http://www.mikroe.com/eng/products/view/792/easymx-pro-v7-for-stellaris-arm/
                      ac:tft_touchpanel

                      ac:seri
     Oscillator:      HS-PLL, 80.000 MHz Crystal
     SW:              mikroC PRO for ARM
                      http://www.mikroe.com/eng/products/view/752/mikroc-pro-for-arm/s
* NOTES:
*     Serial Flash use SPI bus to communicate with MCU.
      Turn on SW11 and SW12 TFT control switches
*     Turn on SW13.1, SW13.2, sw13.3 and SW13.6
**************************************************************************************************/

/**************************************************************************************************
* Serial Flash Chip Select connection
**************************************************************************************************/
#ifndef _SF_FLASH_H
#define _SF_FLASH_H

#define CS_Serial_Flash_bit           GPIOB_ODR.B11
#define CS_Serial_Flash_Direction_bit GPIO_PORTC_DIR.B11
//extern sbit CS_Serial_Flash_bit;
//extern sbit CS_Serial_Flash_Direction_bit;
/*************************************************************************************************/

#define M25P80_ID                                                                                  0x00202014
#define SST26VF064B_ID                                                                             0x00BF2643

// Constans
static const unsigned short _SERIAL_FLASH_CMD_RDID  = 0x9F;    // 25P80
static const unsigned short _SERIAL_FLASH_CMD_READ  = 0x03;
static const unsigned short _SERIAL_FLASH_CMD_WRITE = 0x02;
static const unsigned short _SERIAL_FLASH_CMD_WREN  = 0x06;
static const unsigned short _SERIAL_FLASH_CMD_RDSR  = 0x05;
static const unsigned short _SERIAL_FLASH_CMD_ERASE = 0xC7;    // 25P80
static const unsigned short _SERIAL_FLASH_CMD_EWSR  = 0x06;    // 25P80
static const unsigned short _SERIAL_FLASH_CMD_WRSR  = 0x01;
static const unsigned short _SERIAL_FLASH_CMD_SER   = 0xD8;    //25P80
static const unsigned short _SERIAL_FLASH_CMD_ERASE_SECTOR   = 0x20;    //SST26VF064
static const unsigned short _SERIAL_FLASH_CMD_READ_BPR   = 0x72;    //SST26VF064
static const unsigned short _SERIAL_FLASH_CMD_READ_CR   = 0x35;    //SST26VF064
static const unsigned short _SERIAL_FLASH_CMD_RESET_EN   = 0x66;
static const unsigned short _SERIAL_FLASH_CMD_RESET   = 0x99;
static const unsigned short _SERIAL_FLASH_CMD_GLOBAL_UNLOCK   = 0x98;

extern unsigned long flash_id;
extern unsigned char protection_reg[18];
// Functions
void SerialFlash_init();
void SerialFlash_WriteEnable();
unsigned char SerialFlash_IsWriteBusy();
void SerialFlash_WriteByte(unsigned char _data, unsigned long address);
void SerialFlash_WriteWord(unsigned int _data, unsigned long address);
unsigned long SerialFlash_ReadID(void);
unsigned char SerialFlash_ReadByte(unsigned long address);
unsigned int SerialFlash_ReadWord(unsigned long address);
unsigned char SerialFlash_WriteArray(unsigned long address, unsigned char* pData, unsigned int nCount);
void SerialFlash_ReadArray(unsigned long address, unsigned char* pData, unsigned int nCount);
void SerialFlash_ChipErase(void);
void SerialFlash_ResetWriteProtection();
void SerialFlash_SectorErase(unsigned long address);
void SerialFlash_WriteData(unsigned long address, unsigned char *pData, unsigned long len, unsigned char fill);
unsigned char SerialFlash_ReadConfigReg(void);
void SerialFlash_ReadBlockProt(void);
unsigned char SerialFlash_ReadSR();

/**************************************************************************************************
* End of File
**************************************************************************************************/
#endif